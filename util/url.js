import { URL, URLSearchParams } from 'url';
// URL and URLSearchParams are available on the window object in browsers
// but we have to import them on the server side
export default {
  URL: typeof window !== 'undefined' ? window.URL : URL,
  URLSearchParams: typeof window !== 'undefined' ? window.URLSearchParams : URLSearchParams,
};
