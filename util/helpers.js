/* eslint-disable camelcase */
import fetch from 'isomorphic-unfetch';
import { pickBy, identity } from 'lodash/fp';
import env from './env';
import url from './url';

const {
  FOURSQUARE_CLIENT_ID: client_id,
  FOURSQUARE_CLIENT_SECRET: client_secret,
} = env;

export const setRecentSearches = (recentSearches) => {
  window.localStorage.setItem('recentSearches', JSON.stringify(recentSearches));
};

export const getRecentSearches = () => {
  if (typeof window !== 'undefined' && window.localStorage.getItem('recentSearches')) {
    return JSON.parse(window.localStorage.getItem('recentSearches'));
  }
  return [];
};

const explore = (options) => {
  // URL & URLSearchParams constructors share the same API on node/browsers
  const base = new url.URL('/v2/venues/explore', 'https://api.foursquare.com');
  const search = new url.URLSearchParams({
    client_id,
    client_secret,
    v: 20170801,
    venuePhotos: 1,
    limit: 10,
    ...pickBy(identity, options),
  });
  base.search = search;
  return base.toString();
};

const photos = ({ id }) => {
  const base = new url.URL(`/v2/venues/${id}/photos`, 'https://api.foursquare.com');
  const search = new url.URLSearchParams({
    client_id,
    client_secret,
    v: 20170801,
    limit: 12,
  });
  base.search = search;
  return base.toString();
};

const details = ({ id }) => {
  const base = new url.URL(`/v2/venues/${id}`, 'https://api.foursquare.com');
  const search = new url.URLSearchParams({
    client_id,
    client_secret,
    v: 20170801,
  });
  base.search = search;
  return base.toString();
};

const consumer = async (endpoint) => {
  try {
    const response = await fetch(endpoint);
    return response.json();
  } catch (e) {
    return null;
  }
};

export const fourSquareAPI = {
  explore: opts => consumer(explore(opts)),
  photos: opts => consumer(photos(opts)),
  details: opts => consumer(details(opts)),
};
