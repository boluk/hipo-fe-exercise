# Hipo Frontend Exercise v2.0 - Kaan Boluk

This is a single page application of a Foursquare client based on the specs provided by Hipo. A deployment lives at [hipo.kaan.io](https://hipo.kaan.io). Some features:

* Server Side Rendering (with [next.js](https://github.com/zeit/next.js))
* CSS-in-JS (with [styled-components](https://www.styled-components.com))
* Custom font loader to cache fonts in localStorage
* Simple HTML5 Geolocation API implementation
* Retains basic functionality in a js-disabled environment (thanks to server side rendering)
* SVG filters, SVG rendering React components

## Install & Run

The project was developed on node `v8.6.0` and deployed on `v8.9.4`. It hasn't been tested on earlier versions.

Install dependencies.
```
npm install
```

For development,  run:
```
npm run dev
```

For a production build you will need to provide Foursquare API Client Id and Secret as env variables (more on that below):
```
npm run build
npm start
```


## Build

I used two babel plugins in addition to the next.js preset:

* `babel-plugin-inline-react-svg` enables importing SVG's as react components.

* `babel-plugin-lodash` helps with treeshaking.

## Environment Variables

I used now's `package.json` config to inject these to my deployment. npm script for development exports its own. You will need to provide them if you want a production build from scratch.

`NODE_ENV` determines whether we are building for prod or not.

`FOURSQUARE_CLIENT_ID` Provides the Foursquare Client ID to the app, needed for making API calls.

`FOURSQUARE_CLIENT_SECRET` Provides the Foursquare Client Secret to the app, needed for making API calls.


## Project Structure

I relied on the httpServer and the file-system based routing provided by next.js.

As a result, the app's routes are defined as files in the `pages` folder (except for `_document.js`, which serves as a template for the others).

Rest of the React Components (most of them extends PureComponent, although there are some functional ones which does not rely on either props or state) are stored in the `components` folder. styled-components generated ones are confined to `*.styles.js` files.

`util` folder houses the helper functions and exports which differ by the environment the code executes. I usually prefer creating larger modules of this sort as npm packages and let them manage their browser output internally but I think this is a fine compromise for the task at hand.


## Data fetching

I relied on next.js' getInitialProps lifecycle method to fetch data between pages and submit/click handlers for same-page requests.

I used `isomorphic-unfetch` to provide a unified way of doing things.

## CSS

All necessary css is injected to the `<head>` of the document which creates a snappy experience (initial html response makes instant sense visually) while sparing the hassle of extracting above-the-fold css (and defining a fold). The cost is obviously the larger initial document but I'm fine with the trade-off (especially on an app of this scale).

## Linting

I added `eslint` and `eslint-config-airbnb` (and their peers) as devDependencies and followed them as best as I could. You will see several warnings about console statements (we don't have another logging mechanism) and dangerously set html (a necessity on the server side) if you run
```
npm run lint
```
