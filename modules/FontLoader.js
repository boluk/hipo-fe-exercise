const FontLoader = (cdnHost) => {
  const md5 = 'da437786a7251fae48aeb1f6f77b410c';
  const key = 'hipoFont';
  const insertFont = (value) => {
    const style = document.createElement('style');
    style.innerHTML = value;
    document.head.appendChild(style);
  };
  let cache = false;
  try {
    cache = window.localStorage.getItem(key);
    if (cache) {
      try {
        cache = JSON.parse(cache);
        if (cache.md5 === md5) {
          insertFont(cache.value);
        } else {
          window.localStorage.removeItem(key);
          cache = null;
        }
      } catch (e) {
        window.localStorage.removeItem(key);
      }
    }
  } catch (e) {
    console.log(e);
  }
  if (!cache) {
    /* Fonts not in LocalStorage or md5 did not match */
    window.addEventListener('load', () => {
      fetch(`${cdnHost}/static/font.${md5}.json`).then((response) => {
        response.json().then((json) => {
          insertFont(json.value);
          window.localStorage.setItem(key, window.JSON.stringify(json));
        }).catch(e => console.error(e));
      }).catch(e => console.error(e));
    });
  }
};

export default FontLoader;
