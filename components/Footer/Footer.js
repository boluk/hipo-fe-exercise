import React from 'react';
import { FooterNav, FooterLink } from './Footer.styles';

const Footer = () => (
  <FooterNav role="navigation">
    <FooterLink href="https://hipolabs.com/team/" rel="noopener" target="_blank">
      About Us
    </FooterLink>
    <FooterLink href="mailto:hello@hipolabs.com">Contact</FooterLink>
    <FooterLink href="https://medium.com/@hipolabs" rel="noopener" target="_blank">
      Blog
    </FooterLink>
  </FooterNav>
);

export default Footer;
