import styled from 'styled-components';

export const FooterNav = styled.nav`
  height: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const FooterLink = styled.a`
  text-decoration: none;
  color: #9b9b9b;
  font-size: 12px;
  font-weight: 500;
  margin: 0 10px;
  outline: 0;
`;
