import React from 'react';
import PropTypes from 'prop-types';

// using an SVG filter enables us to use Foursquare supplied .png category logos
// in the color we want
class CategoryLogo extends React.PureComponent {
  render() {
    const { url } = this.props;
    return (
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="60" height="90">
        <defs>
          <filter id="colorMask">
            <feFlood floodColor="#FFC5C5" result="flood" />
            <feComposite
              in="SourceGraphic"
              in2="flood"
              operator="arithmetic"
              k1="1"
              k2="0"
              k3="0"
              k4="0"
            />
          </filter>
        </defs>
        <image width="100%" height="100%" xlinkHref={url} filter="url(#colorMask)" />
      </svg>
    );
  }
}

CategoryLogo.propTypes = {
  url: PropTypes.string.isRequired,
};

export default CategoryLogo;
