import React from 'react';
import PropTypes from 'prop-types';
import { VenuePhotoOverlay, VenuePhotoUserImage } from './Venue.styles';
import { ResultListItem, ResultPhoto } from '../styles';

class PhotoItem extends React.PureComponent {
  render() {
    const {
      firstName, lastName, photo, prefix, suffix,
    } = this.props;
    return (
      <ResultListItem>
        <ResultPhoto src={`${prefix}300x300${suffix}`} />
        <VenuePhotoOverlay>
          <VenuePhotoUserImage src={`${photo.prefix}100x100${photo.suffix}`} />
          {`${firstName}${lastName ? ` ${lastName}` : ''}`}
        </VenuePhotoOverlay>
      </ResultListItem>
    );
  }
}

PhotoItem.propTypes = {
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string,
  photo: PropTypes.shape({
    suffix: PropTypes.string.isRequired,
    prefix: PropTypes.string.isRequired,
  }).isRequired,
  prefix: PropTypes.string.isRequired,
  suffix: PropTypes.string.isRequired,
};

PhotoItem.defaultProps = {
  lastName: '',
};

export default PhotoItem;
