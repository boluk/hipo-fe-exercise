import React from 'react';
import PropTypes from 'prop-types';
import { VenueTip, TipUser, TipAvatar, TipUserName, TipText } from './Venue.styles';

class Tip extends React.PureComponent {
  render() {
    const {
      firstName, text, prefix, suffix,
    } = this.props;
    return (
      <VenueTip>
        <TipUser>
          <TipAvatar src={`${prefix}100x100${suffix}`} />
          <TipUserName>{firstName}</TipUserName>
        </TipUser>
        <TipText>{text}</TipText>
      </VenueTip>
    );
  }
}

Tip.propTypes = {
  firstName: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  prefix: PropTypes.string.isRequired,
  suffix: PropTypes.string.isRequired,
};

export default Tip;
