import React from 'react';
import PropTypes from 'prop-types';
import {
  VenueDetailsWrapper,
  Address,
  AddressIcon,
  Phone,
  PhoneIcon,
  VenueScore,
} from './Venue.styles';
import { TagIcon, Scale, UserCount, UserIcon } from '../styles';

class VenueDetails extends React.PureComponent {
  render() {
    const {
      rating, phone, usersCount, address, price,
    } = this.props;
    return (
      <VenueDetailsWrapper>
        <AddressIcon />
        <Address>{address}</Address>
        <PhoneIcon />
        <Phone>{phone}</Phone>
        <UserIcon />
        <UserCount>{usersCount}</UserCount>
        {price ? (
          <React.Fragment>
            <TagIcon />
            <Scale price={price} />
          </React.Fragment>
        ) : null}
        {rating ? <VenueScore score={rating} /> : null}
      </VenueDetailsWrapper>
    );
  }
}

VenueDetails.propTypes = {
  rating: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  phone: PropTypes.string.isRequired,
  usersCount: PropTypes.number.isRequired,
  address: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
};

export default VenueDetails;
