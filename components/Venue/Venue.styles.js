import styled from 'styled-components';
import AddressSvg from '../../static/address.svg';
import PhoneSvg from '../../static/phone.svg';
import LogoSvg from '../../static/logo.svg';
import { Grid, SideBar, SideBarListItem } from '../styles';

export const VenueLogo = styled(LogoSvg)`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
`;

export const CategoryLogoContainer = styled.div`
  position: absolute;
  top: 146px;
  left: 405px;
  height: 178px;
  width: 178px;
  transform: rotate(-45deg);
  border: 3.5px solid #ffc5c5;
  svg {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%) rotate(45deg);
  }
`;

export const VenueHeader = styled.header`
  height: 600px;
  display: flex;
  flex-direction: column-reverse;
  width: 100%;
  ${props =>
    (props.bgImage
      ? `
    background-image: url(${props.bgImage});
    background-size: cover;
  `
      : '')}
  position: relative;
  &:before {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    content: '';
    background-color: #12195f;
    opacity: 0.6;
  }
`;

export const VenueTitle = styled.h1`
  margin: 0 auto 28px;
  width: 930px;
  padding: 0 15px;
  font-size: 64px;
  font-weight: 300;
  color: #fff;
  line-height: normal;
  position: relative;
`;

export const VenueDetailsContainer = styled.footer`
  flex: 0 0 115px;
  position: relative;
  &:after {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    opacity: 0.8;
    z-index: 1;
    content: '';
    background-image: linear-gradient(95deg, #1e0db4, #e44747);
  }
`;

export const VenueDetailsWrapper = Grid.extend`
  position: relative;
  height: 100%;
  display: grid;
  padding: 24px 0;
  grid-template:
    [row1-start] 'address-icon address address address' 12px [row1-end]
    [row2-start] 'phone-icon phone phone phone' 12px [row2-end]
    [row3-start] 'user-icon user-count tag-icon scale' 12px [row3-end]
    / 10px 57px 10px auto;
  grid-gap: 18px 8px;
  color: #fff;
  font-size: 12px;
  font-weight: 500;
  z-index: 2;
`;

export const AddressIcon = styled(AddressSvg)`
  grid-area: address-icon;
  align-self: end;
`;

export const Address = styled.span`
  grid-area: address;
`;

export const PhoneIcon = styled(PhoneSvg)`
  grid-area: phone-icon;
  align-self: end;
`;

export const Phone = styled.span`
  grid-area: phone;
`;

export const VenueScore = styled.div`
  &:before {
    position: absolute;
    top: 50%;
    left: 50%;
    content: '${props => props.score}';
    transform: translate(-50%, -50%);
    font-weight: 400;
    font-size: 18px;
  }
  position: absolute;
  right: 0;
  top: 0;
  width: 90px;
  height: 90px;
  clip-path: polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%);
  transform: translate(0, -50%);
  background: #776cee;
`;

export const VenuePhotoOverlay = styled.div`
  display: none;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(18, 25, 95, 0.5);
  color: #fff;
  li:hover & {
    display: flex;
  }
`;

export const VenuePhotoUserImage = styled.img`
  height: 48px;
  width: 48px;
  margin-bottom: 12px;
  clip-path: polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%);
`;

export const VenueTips = SideBar.extend`
  position: relative;
  top: -70px;
  box-shadow: 0 0 34px 0 rgba(115, 95, 255, 0.32);
  z-index: 2;
`;

export const VenueTip = SideBarListItem.extend`
  flex: 1 0 100px;
`;

export const VenueTipShowMore = VenueTip.extend`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const VenueTipShowMoreLink = styled.a`
  color: #7a74d2;
  font-size: 11px;
  font-weight: 500;
  text-decoration: underline;
  cursor: pointer;
`;

export const TipUser = styled.figure`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 20px 0 0;
  padding: 0;
`;

export const TipUserName = styled.figcaption`
  flex: 0 0 180px;
  font-size: 14px;
  font-weight: 500;
`;

export const TipAvatar = styled.img`
  height: 56px;
  width: 56px;
  clip-path: polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%);
`;

export const TipText = styled.p`
  margin: 0 0 20px;
  padding: 0 0 0 80px;
  font-size: 14px;
  font-weight: 400;
`;
