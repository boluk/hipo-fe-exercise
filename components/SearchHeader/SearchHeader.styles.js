import styled from 'styled-components';
import { Grid } from '../styles';
import LogoOuterSvg from '../../static/logo-outer.svg';
import LogoInnerSvg from '../../static/logo-inner.svg';

export const LogoFrame = styled(LogoOuterSvg)`
  position: absolute;
  top: 0;
  left: 0;
  transition: all 0.4s ease-out;
  ${props =>
    (props.compact
      ? 'transform: translate(-101px, -104px);'
      : 'transform: translate(238px, 0px);'
    )};
`;

export const Logo = styled(LogoInnerSvg)`
  position: absolute;
  top: 0;
  left: 0;
  transition: all 0.4s ease-out;
  ${props =>
    (props.compact
      ? `
    path {
      fill: #fff;
    }
    transform: translate(102px, 23px);
  `
      : `
    transform: translate(441px, 67px);
  `)};
`;

export const HeaderWrapper = styled.section`
  transition: all 0.4s ease-out;
  display: flex;
  ${props =>
    (props.compact
      ? `
    height: 150px;
    &:before {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      content: '';
      background-image: linear-gradient(353deg, #1e0db4, #e44747);
      opacity: .8;
    }
  `
      : `
    height: 600px;
  `)};
  background-image: url(${props => props.bgImage});
  background-size: cover;
  position: relative;
`;

export const HeaderGrid = Grid.extend`
  position: relative;
  display: flex;
  ${props =>
    (props.compact
      ? `
    align-items: center;
  `
      : `
    padding-top: 251px;
    align-items: center;
    flex-direction: column;
  `)};
`;

export const Heading = styled.h1`
  font-size: 55px;
  font-weight: 200;
  text-align: center;
  color: #fff;
  margin: 0 0 17px;
`;

export const SubHeading = styled.h2`
  font-size: 16px;
  font-weight: 400;
  text-align: center;
  width: 543px;
  margin: 0 auto 65px;
  color: #fff;
`;
