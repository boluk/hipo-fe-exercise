import React from 'react';
import PropTypes from 'prop-types';
import SearchForm from '../SearchForm';
import {
  HeaderWrapper,
  HeaderGrid,
  Heading,
  SubHeading,
  LogoFrame,
  Logo,
} from './SearchHeader.styles';

// Keeping the frame and the cutlery as seperate components
// help with positioning/animation
class SearchHeader extends React.PureComponent {
  render() {
    const {
      compact, formSubmit, inputChange, currentSearch, requireNear,
    } = this.props;
    return (
      <HeaderWrapper bgImage="/static/background.jpg" compact={compact}>
        <HeaderGrid compact={compact}>
          {compact ? null : (
            <React.Fragment>
              <Heading>Lorem ipsum dolor sit!</Heading>
              <SubHeading>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua.
              </SubHeading>
            </React.Fragment>
          )}

          <LogoFrame compact={compact ? 'true' : ''} />
          <Logo compact={compact ? 'true' : ''} />
          <SearchForm
            formSubmit={formSubmit}
            inputChange={inputChange}
            initialValues={currentSearch}
            compact={compact}
            requireNear={requireNear}
          />
        </HeaderGrid>
      </HeaderWrapper>
    );
  }
}

SearchHeader.propTypes = {
  compact: PropTypes.bool.isRequired,
  formSubmit: PropTypes.func.isRequired,
  inputChange: PropTypes.func.isRequired,
  currentSearch: PropTypes.shape({
    query: PropTypes.string,
    near: PropTypes.string,
  }),
  requireNear: PropTypes.bool,
};

SearchHeader.defaultProps = {
  currentSearch: {},
  requireNear: true,
};

export default SearchHeader;
