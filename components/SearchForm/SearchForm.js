import React from 'react';
import PropTypes from 'prop-types';
import { QueryInput, LocationInput, SearchButton, SearchIcon, Form } from './SearchForm.styles';

// Good old fashioned action attribute helps serve the folk without js.
// On the other hand, if we have user coordinates, location input
// should not be required.

class SearchForm extends React.PureComponent {
  render() {
    const {
      formSubmit, inputChange, initialValues, compact, requireNear,
    } = this.props;
    return (
      <Form action="search" method="get" onSubmit={formSubmit} compact={compact}>
        <QueryInput
          placeholder={initialValues.query || 'I am looking for'}
          name="query"
          onChange={inputChange}
        />
        <LocationInput
          placeholder={initialValues.near || 'place'}
          name="near"
          onChange={inputChange}
          required={requireNear}
        />
        <SearchButton type="submit">
          <SearchIcon />
        </SearchButton>
      </Form>
    );
  }
}

SearchForm.propTypes = {
  compact: PropTypes.bool.isRequired,
  formSubmit: PropTypes.func.isRequired,
  inputChange: PropTypes.func.isRequired,
  initialValues: PropTypes.shape({
    query: PropTypes.string,
    near: PropTypes.string,
  }).isRequired,
  requireNear: PropTypes.bool.isRequired,
};

export default SearchForm;
