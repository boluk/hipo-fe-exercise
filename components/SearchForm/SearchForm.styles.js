import styled from 'styled-components';
import MagGlass from '../../static/shape.svg';

export const SearchIcon = styled(MagGlass)``;

export const QueryInput = styled.input`
  box-sizing: border-box;
  flex: 0 0 285px;
  height: 45px;
  border: 0;
  border-radius: 4px;
  background-color: #ffffff;
  box-shadow: 0 12px 21px 0 rgba(0, 0, 0, 0.24);
  font-size: 14px;
  font-weight: 500;
  line-height: 45px;
  padding: 0 20px;
  color: #4a4a4a;
`;

export const LocationInput = QueryInput.extend`
  flex: 0 0 156px;
  max-width: 156px;
  margin-left: 10px;
`;

export const SearchButton = styled.button`
  width: 77px;
  height: 45px;
  border-radius: 4px;
  background-color: #ff5f5f;
  line-height: 45px;
  border: 0;
  margin: 0 0 0 10px;
  box-shadow: 0 12px 21px 0 rgba(0, 0, 0, 0.24);
  position: relative;
`;

export const Form = styled.form`
  display: flex;
  transition: all 0.4s ease-out;
  ${props =>
    (props.compact
      ? `
    margin: 0 0 0 310px;
  `
      : `
    margin: 0;
    width: 100%;
    justify-content: center;
  `)};
`;
