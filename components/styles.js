import styled from 'styled-components';
import User from '../static/user.svg';
import Tag from '../static/tag.svg';

export const globalStyles = `
  body {
    background-color: #fff;
    font-family: Gotham;
    font-weight: 400;
    margin: 0;
  }
`;

export const Grid = styled.section`
  width: 930px;
  margin: 0 auto;
`;

export const TwoColGrid = Grid.extend`
  margin-top: 20px;
  display: grid;
  grid-template-columns: auto 290px;
  grid-gap: 20px;
  align-items: start;
`;

export const ResultList = styled.ol`
  list-style-type: none;
  display: grid;
  grid-template-columns: 290px 290px;
  grid-gap: 20px;
  margin: 0;
  padding: 0;
  grid-column: 1 / 2;
`;

export const ResultListItem = styled.li`
  position: relative;
  height: 290px;
`;

export const ResultPhoto = styled.img`
  max-width: 100%;
`;

export const SideBar = styled.aside`
  background: #fff;
  box-shadow: 0 0 34px 0 rgba(255, 95, 95, 0.32);
  padding: 0 15px;
  grid-column: 2 / 3;
`;

export const SideBarList = styled.ol`
  list-style-type: none;
  padding: 0;
  margin: 0;
  display: flex;
  flex-direction: column;
`;

export const SideBarListItem = styled.li`
  border-bottom: 1px solid #e4e4e4;
  color: #9b9b9b;
  &:last-of-type {
    border: 0;
  }
`;

export const SideBarHeader = styled.h2`
  margin: 0 auto 16px;
  text-align: center;
  padding: 39px 0 27px;
  line-height: 1;
  font-size: 16px;
  font-weight: 500;
  color: #4a4a4a;
  position: relative;
  &:after {
    position: absolute;
    content: '';
    width: 70px;
    height: 2px;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%);
    background: #c4c0ff;
  }
`;

export const UserIcon = styled(User)`
  grid-area: user-icon;
  align-self: end;
`;

export const UserCount = styled.span`
  grid-area: user-count;
`;

export const TagIcon = styled(Tag)`
  grid-area: tag-icon;
  align-self: end;
`;

export const Scale = styled.span`
  grid-area: scale;
  align-self: center;
  height: 3px;
  width: 60px;
  position: relative;
  background: linear-gradient(
    to right,
    rgba(201, 197, 255, 1) 0%,
    rgba(201, 197, 255, 1) 25%,
    rgba(165, 159, 238, 1) 25%,
    rgba(165, 159, 238, 1) 50%,
    rgba(139, 129, 255, 1) 50%,
    rgba(139, 129, 255, 1) 75%,
    rgba(104, 93, 234, 1) 75%,
    rgba(104, 93, 234, 1) 100%
  );
  &:after {
    background: #000;
    position: absolute;
    top: 0;
    right: 0;
    height: 3px;
    width: ${props => (4 - props.price) * 25}%;
    content: '';
  }
`;
