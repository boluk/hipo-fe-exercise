import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { VenueResultItem, VenueResultLink, VenueTitle, VenueStats } from './Search.styles';
import { TagIcon, Scale, UserCount, UserIcon } from '../styles';

class SearchResultItem extends React.PureComponent {
  render() {
    const {
      bgImage, id, name, usersCount, price,
    } = this.props;
    return (
      <VenueResultItem bgImage={bgImage}>
        <Link
          prefetch
          href={{
            pathname: '/venue',
            query: {
              id,
            },
          }}
          passHref
        >
          <VenueResultLink>
            <VenueStats>
              <UserIcon />
              <UserCount>{usersCount}</UserCount>
              {price ? (
                <React.Fragment>
                  <TagIcon />
                  <Scale price={price} />
                </React.Fragment>
              ) : null}
            </VenueStats>
            <VenueTitle>{name}</VenueTitle>
          </VenueResultLink>
        </Link>
      </VenueResultItem>
    );
  }
}

SearchResultItem.propTypes = {
  bgImage: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  usersCount: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
};

export default SearchResultItem;
