import styled from 'styled-components';
import { ResultListItem, SideBarListItem, TwoColGrid } from '../styles';

export const SearchGrid = TwoColGrid.extend`
  margin-top: 40px;
`;

export const VenueResultItem = ResultListItem.extend`
  ${props =>
    (props.bgImage
      ? `
    background: url(${props.bgImage});
  `
      : '')}
  &:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="290" height="290" viewBox="0 0 290 290"><defs><path id="a" d="M0 0h290v290H0z"/></defs><use fill="#12195F" fill-rule="evenodd" opacity=".5" xlink:href="#a"/></svg>');
    z-index: 2;
  }
`;

export const VenueResultLink = styled.a`
  display: flex;
  flex-direction: column-reverse;
  height: 100%;
  position: relative;
  z-index: 3;
  cursor: pointer;
  padding: 20px;
  color: #fff;
  text-decoration: none;
  box-sizing: border-box;
`;

export const VenueTitle = styled.h3`
  font-size: 25px;
  font-weight: 400;
  margin: 0;
  padding: 0 0 10px;
  position: relative;
  &:after {
    position: absolute;
    content: '';
    bottom: 0;
    left: 0;
    width: 48px;
    height: 2px;
    background: #c4c0ff;
  }
`;

export const VenueStats = styled.section`
  margin-top: 13px;
  font-size: 12px;
  font-weight: 500;
  display: grid;
  grid-template:
    [row1-start] 'user-icon user-count tag-icon scale' 12px [row1-end]
    / 10px 57px 10px auto;
  grid-gap: 18px 8px;
`;

export const RecentSearch = SideBarListItem.extend`
  flex: 1 0 50px;
  font-size: 14px;
  font-weight: 500;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

export const NoResultHeader = styled.h3`
  text-align: center;
  color: #4a4a4a;
`;
export const NoResultSubHead = styled.h4`
  text-align: center;
  color: #4a4a4a;
`;
export const NoResultPrevList = styled.ol`
  display: flex;
  flex-wrap: wrap;
  margin: 0;
  padding: 0;
  list-style-type: none;
`;
export const NoResultPrev = styled.li`
  flex: 0 0 auto;
  margin: 0 10px 10px;
  display: flex;
`;
export const NoResultPrevLink = styled.a`
  box-sizing: border-box;
  padding: 4px 5px;
  height: 100%;
  line-height: 30px;
  text-decoration: none;
  cursor: pointer;
  box-shadow: 0 0 3px 0 rgba(115, 95, 255, 0.82);
  color: #1e0db4;
  border-radius: 4px;
  transition: all 0.3s ease-in;
  &:hover {
    box-shadow: 0 0 24px 0 rgba(115, 95, 255, 0.32);
  }
`;
