import React from 'react';
import PropTypes from 'prop-types';
import { map, isEmpty } from 'lodash/fp';
import SearchResultItem from './SearchResultItem';
import NoResults from './NoResults';
import { ResultList, SideBar, SideBarList, SideBarHeader } from '../styles';
import { RecentSearch, SearchGrid } from './Search.styles';

class SearchResults extends React.PureComponent {
  render() {
    const {
      venues, recentSearches, handleHistory, currentSearch,
    } = this.props;
    if (isEmpty(venues)) {
      return (
        <NoResults
          currentSearch={currentSearch}
          recentSearches={recentSearches}
          handleHistory={handleHistory}
        />
      );
    }
    return (
      <SearchGrid>
        <ResultList>
          {map(
            ({
 venue: {
 id, name, photos: { groups }, stats: { usersCount }, price,
},
}) => (
  <SearchResultItem
    id={id}
    name={name}
    usersCount={usersCount}
    bgImage={
                  groups.length
                    ? `${groups[0].items[0].prefix}300x300${groups[0].items[0].suffix}`
                    : ''
                }
    price={price ? price.tier : 0}
    key={id}
  />
            ),
            venues,
          )}
        </ResultList>
        <SideBar>
          <SideBarHeader>RECENT SEARCHES</SideBarHeader>
          <SideBarList>
            {isEmpty(recentSearches)
              ? null
              : map(
                  ({ query, near, requestId }) => (
                    <RecentSearch key={requestId} onClick={() => handleHistory({ query, near })}>
                      {`${query} ${near ? `in ${near}` : 'around you'}`}
                    </RecentSearch>
                  ),
                  recentSearches,
                )}
          </SideBarList>
        </SideBar>
      </SearchGrid>
    );
  }
}

SearchResults.propTypes = {
  venues: PropTypes.arrayOf(PropTypes.shape({
    venue: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
    }),
    stats: PropTypes.shape({
      usersCount: PropTypes.number,
    }),
    price: PropTypes.shape({
      tier: PropTypes.number,
    }),
    photos: PropTypes.shape({
      groups: PropTypes.arrayOf(PropTypes.shape({
        items: PropTypes.arrayOf(PropTypes.shape({
          id: PropTypes.string,
          prefix: PropTypes.string,
          suffix: PropTypes.string,
        })),
      })),
    }),
  })).isRequired,
  recentSearches: PropTypes.arrayOf(PropTypes.shape({
    requestId: PropTypes.string.isRequired,
    near: PropTypes.string,
    query: PropTypes.string,
  })).isRequired,
  currentSearch: PropTypes.shape({
    query: PropTypes.string,
    near: PropTypes.string,
  }).isRequired,
  handleHistory: PropTypes.func.isRequired,
};

export default SearchResults;
