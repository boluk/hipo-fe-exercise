import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash/fp';
import { Grid } from '../styles';
import {
  NoResultHeader,
  NoResultSubHead,
  NoResultPrev,
  NoResultPrevList,
  NoResultPrevLink,
} from './Search.styles';

class NoResults extends React.PureComponent {
  render() {
    const {
      recentSearches,
      currentSearch: { query: currentSearchQuery, near: currentSearchLocation },
      handleHistory,
    } = this.props;

    return (
      <Grid>
        <NoResultHeader>
          {`We couldn't find any results for "${currentSearchQuery}" ${
            currentSearchLocation ? `in ${currentSearchLocation}` : 'around you'
          }.`}
        </NoResultHeader>
        <NoResultSubHead>
          You can go back to one of your recent searches below or try a new one.
        </NoResultSubHead>
        <NoResultPrevList>
          {map(
            ({ query, near, requestId }) => (
              <NoResultPrev key={requestId}>
                <NoResultPrevLink onClick={() => handleHistory({ query, near })}>
                  {`${query} ${near ? `in ${near}` : 'around you'}`}
                </NoResultPrevLink>
              </NoResultPrev>
            ),
            recentSearches,
          )}
        </NoResultPrevList>
      </Grid>
    );
  }
}

NoResults.propTypes = {
  recentSearches: PropTypes.arrayOf(PropTypes.shape({
    requestId: PropTypes.string.isRequired,
    near: PropTypes.string,
    query: PropTypes.string,
  })).isRequired,
  currentSearch: PropTypes.shape({
    query: PropTypes.string,
    near: PropTypes.string,
  }).isRequired,
  handleHistory: PropTypes.func.isRequired,
};

export default NoResults;
