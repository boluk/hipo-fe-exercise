import React from 'react';
import PropTypes from 'prop-types';
import { take, getOr } from 'lodash/fp';
import Router from 'next/router';
import Footer from '../components/Footer';
import SearchHeader from '../components/SearchHeader';
import SearchResults from '../components/Search/SearchResults';
import { getRecentSearches, setRecentSearches, fourSquareAPI } from '../util/helpers';

class Search extends React.Component {
  static async getInitialProps({ query: { query = '', near, ll } }) {
    if (near || ll) {
      const explore = await fourSquareAPI.explore({ query, near, ll });
      return { explore };
    }
    return { explore: '' };
  }

  constructor(props) {
    super(props);
    const { explore, url: { query } } = this.props;
    this.state = {
      requestId: getOr('', 'meta.requestId', explore),
      venues: getOr([], 'response.groups[0].items', explore),
      recentSearches: getRecentSearches(),
      currentSearch: { ...query },
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if (typeof window !== 'undefined' && 'geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          this.setState({
            ll: `${position.coords.latitude},${position.coords.longitude}`,
          });
        },
        e => console.error(e.code),
        { timeout: 10 * 1000, maximumAge: 5 * 60 * 1000 },
      );
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.requestId === this.state.requestId;
  }

  handleHistory = pastSearch => this.setState({ ...pastSearch }, () => this.handleSubmit());

  async handleSubmit(e = {}) {
    if (e.target instanceof HTMLFormElement) {
      e.preventDefault();
    }
    const {
      query, near, recentSearches, ll,
    } = this.state;
    const { url: { pathname } } = this.props;
    const explore = await fourSquareAPI.explore({ query, near, ll });
    const requestId = getOr('', 'meta.requestId', explore);
    const status = getOr(500, 'meta.code', explore);
    if (requestId && status === 200) {
      const href = `${pathname}?near=${near}&query=${query}`;
      this.setState(
        {
          requestId,
          venues: getOr([], 'response.groups[0].items', explore),
          currentSearch: { query, near },
          recentSearches: [{ query, near, requestId }, ...take(9, recentSearches)],
        },
        () => {
          Router.replace(href, href, { shallow: true });
          setRecentSearches(this.state.recentSearches);
        },
      );
    }
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  render() {
    const {
      venues, recentSearches, currentSearch, ll,
    } = this.state;
    return (
      <React.Fragment>
        <SearchHeader
          compact
          currentSearch={currentSearch}
          formSubmit={this.handleSubmit}
          inputChange={this.handleChange}
          requireNear={!ll}
        />
        <SearchResults
          venues={venues}
          recentSearches={recentSearches}
          currentSearch={currentSearch}
          handleHistory={this.handleHistory}
        />
        <Footer />
      </React.Fragment>
    );
  }
}

Search.propTypes = {
  explore: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      groups: PropTypes.arrayOf(PropTypes.shape({
        items: PropTypes.arrayOf(PropTypes.shape({
          venue: PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
          }),
          stats: PropTypes.shape({
            usersCount: PropTypes.number,
          }),
          price: PropTypes.shape({
            tier: PropTypes.number,
          }),
          photos: PropTypes.shape({
            groups: PropTypes.arrayOf(PropTypes.shape({
              items: PropTypes.arrayOf(PropTypes.shape({
                id: PropTypes.string,
                prefix: PropTypes.string,
                suffix: PropTypes.string,
              })),
            })),
          }),
        })),
      })),
    }),
  ]).isRequired,
  url: PropTypes.shape({
    query: PropTypes.shape({
      query: PropTypes.string,
      near: PropTypes.string,
      ll: PropTypes.string,
    }),
  }).isRequired,
};

export default Search;
