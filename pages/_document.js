import React from 'react';
import { ServerStyleSheet } from 'styled-components';
import Document, { Main, NextScript } from 'next/document';
import { globalStyles } from '../components/styles';
import FontLoader from '../modules/FontLoader';

const cdnHost = process.env.CDN_HOST || '.';

const { FOURSQUARE_CLIENT_ID, FOURSQUARE_CLIENT_SECRET } = process.env;
const env = { FOURSQUARE_CLIENT_ID, FOURSQUARE_CLIENT_SECRET };

export default class HipoDocument extends Document {
  static async getInitialProps({ renderPage }) {
    const page = renderPage();
    return { ...page };
  }

  render() {
    const sheet = new ServerStyleSheet();
    const main = sheet.collectStyles(<Main />);
    const styleTags = sheet.getStyleElement();
    return (
      <html lang="en">
        <head>
          <link rel="icon" type="image/png" href="/static/favicon-32x32.png" sizes="32x32" />
          <link rel="icon" type="image/png" href="/static/favicon-16x16.png" sizes="16x16" />
          <style dangerouslySetInnerHTML={{ __html: globalStyles }} />
          {styleTags}
        </head>
        <body>
          {main}
          {/* this is where we pass process.env to window.ENV */}
          <script dangerouslySetInnerHTML={{ __html: `ENV = ${JSON.stringify(env)}` }} />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                window.FontLoader = ${FontLoader}; FontLoader("${cdnHost}");
            `,
            }}
          />
          <NextScript />
        </body>
      </html>
    );
  }
}
