import React from 'react';
import PropTypes from 'prop-types';
import { flow, split, join, map, getOr, take, find } from 'lodash/fp';
import { fourSquareAPI } from '../util/helpers';
import Tip from '../components/Venue/Tip';
import PhotoItem from '../components/Venue/PhotoItem';
import VenueDetails from '../components/Venue/VenueDetails';
import Footer from '../components/Footer';
import { TwoColGrid, SideBarList, SideBarHeader, ResultList } from '../components/styles';
import {
  VenueHeader,
  VenueLogo,
  CategoryLogoContainer,
  VenueTitle,
  VenueDetailsContainer,
  VenueTips,
  VenueTipShowMore,
  VenueTipShowMoreLink,
} from '../components/Venue/Venue.styles';
import CategoryLogo from '../components/Venue/CategoryLogo';

class Venue extends React.Component {
  static async getInitialProps({ res, query: { id } }) {
    // if we don't have an id, we should just redirect the visitor to
    // somewhere she can search for stuff
    if (id) {
      const [venueDetails, venuePhotos] = await Promise.all([
        fourSquareAPI.details({ id }),
        fourSquareAPI.photos({ id }),
      ]);
      return { venueDetails, venuePhotos };
    }
    res.writeHead(302, { Location: '/' });
    return res.end();
  }

  constructor(props) {
    super(props);
    const { venueDetails, venuePhotos, url: { query: { id } } } = this.props;
    // providing fallback values helps with displaying partial info
    this.state = {
      showAllTips: false,
      tips: getOr([], 'response.venue.tips.groups[0].items', venueDetails),
      photos: getOr([], 'response.photos.items', venuePhotos),
      bestPhoto: this.getBestPhoto(venueDetails),
      address: this.getAddress(venueDetails),
      phone: getOr('', 'response.venue.contact.formattedPhone', venueDetails),
      rating: getOr('', 'response.venue.rating', venueDetails),
      name: getOr(`Venue #${id}`, 'response.venue.name', venueDetails),
      usersCount: getOr(0, 'response.venue.stats.usersCount', venueDetails),
      price: getOr(0, 'response.venue.price.tier', venueDetails),
      category: flow(getOr([], 'response.venue.categories'), find('primary'))(venueDetails),
    };
  }

  getAddress = flow(getOr([], 'response.venue.location.formattedAddress'), join(', '));

  getBestPhoto = flow(getOr({}, 'response.venue.bestPhoto'), ({ prefix, suffix }) => {
    // we should check this in case we got the default {} we set
    if (prefix) {
      return `${prefix}original${suffix}`;
    }
    return false;
  });

  showMoreTips = () => this.setState({ showAllTips: true });

  // flow/compose helps with keeping functions point-free:
  // the array we get from splitting by ' ' is fed into the map
  // to create a new array in which the 2nd element has paranthesis'
  // in order to join by ' ' again to finalize the string
  formatPhone = flow(
    split(' '),
    map.convert({ cap: false })((i, idx) => (idx === 1 ? `(${i})` : i)),
    join(' '),
  );

  render() {
    const {
      tips,
      photos,
      bestPhoto,
      address,
      phone,
      rating,
      name,
      usersCount,
      showAllTips,
      category,
      price,
    } = this.state;
    return (
      <React.Fragment>
        <VenueHeader bgImage={bestPhoto}>
          <VenueDetailsContainer>
            <VenueDetails
              address={address}
              phone={phone ? this.formatPhone(phone) : '-'}
              usersCount={usersCount}
              rating={rating}
              price={price}
            />
          </VenueDetailsContainer>
          <VenueTitle>{name}</VenueTitle>
          {category && (
            <CategoryLogoContainer>
              <CategoryLogo url={`${category.icon.prefix}88${category.icon.suffix}`} />
            </CategoryLogoContainer>
          )}
          <VenueLogo />
        </VenueHeader>
        <TwoColGrid>
          <ResultList>
            {map(
              ({
 id, prefix, suffix, user: { firstName, lastName, photo },
}) => (
  <PhotoItem
    key={id}
    prefix={prefix}
    suffix={suffix}
    firstName={firstName}
    lastName={lastName}
    photo={photo}
  />
              ),
              photos,
            )}
          </ResultList>
          <VenueTips>
            <SideBarHeader>TIPS</SideBarHeader>
            <SideBarList>
              {flow(
                ...(showAllTips ? [] : [take(4)]), // initially show only 4 tips
                map(({ user: { firstName, photo: { prefix, suffix } }, text, id }) => (
                  <Tip prefix={prefix} suffix={suffix} firstName={firstName} text={text} key={id} />
                )),
              )(tips)}
              {showAllTips || tips.length < 5 ? null : (
                <VenueTipShowMore>
                  <VenueTipShowMoreLink onClick={this.showMoreTips}>All Tips</VenueTipShowMoreLink>
                </VenueTipShowMore>
              )}
            </SideBarList>
          </VenueTips>
        </TwoColGrid>
        <Footer />
      </React.Fragment>
    );
  }
}

Venue.propTypes = {
  venueDetails: PropTypes.shape({
    response: PropTypes.shape({
      venue: PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string,
        contact: PropTypes.shape({
          formattedPhone: PropTypes.string,
        }),
        categories: PropTypes.arrayOf(PropTypes.shape({
          primary: PropTypes.bool,
          icon: PropTypes.shape({
            prefix: PropTypes.string,
            suffix: PropTypes.string,
          }),
        })),
        stats: PropTypes.shape({
          usersCount: PropTypes.number,
        }),
        rating: PropTypes.number,
        photos: PropTypes.shape({
          groups: PropTypes.arrayOf(PropTypes.shape({
            items: PropTypes.arrayOf(PropTypes.shape({
              id: PropTypes.string,
              prefix: PropTypes.string,
              suffix: PropTypes.string,
              user: PropTypes.shape({
                firstName: PropTypes.string,
                lastName: PropTypes.string,
                photo: PropTypes.shape({
                  prefix: PropTypes.string,
                  suffix: PropTypes.string,
                }),
              }),
            })),
          })),
        }),
        tips: PropTypes.shape({
          groups: PropTypes.arrayOf(PropTypes.shape({
            items: PropTypes.arrayOf(PropTypes.shape({
              id: PropTypes.string,
              text: PropTypes.string,
              user: PropTypes.shape({
                firstName: PropTypes.string,
                photo: PropTypes.shape({
                  prefix: PropTypes.string,
                  suffix: PropTypes.string,
                }),
              }),
            })),
          })),
        }),
        bestPhoto: PropTypes.shape({
          prefix: PropTypes.string,
          suffix: PropTypes.string,
        }),
      }),
    }),
  }).isRequired,
  venuePhotos: PropTypes.shape({
    response: PropTypes.shape({
      photos: PropTypes.shape({
        items: PropTypes.arrayOf(PropTypes.shape({
          id: PropTypes.string,
          prefix: PropTypes.string,
          suffix: PropTypes.string,
          user: PropTypes.shape({
            firstName: PropTypes.string,
            lastName: PropTypes.string,
            photo: PropTypes.shape({
              prefix: PropTypes.string,
              suffix: PropTypes.string,
            }),
          }),
        })),
      }),
    }),
  }).isRequired,
  url: PropTypes.shape({
    query: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
};

export default Venue;
