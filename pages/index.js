import React from 'react';
import PropTypes from 'prop-types';
import { take, getOr, pickBy, identity, isObject } from 'lodash/fp';
import Router from 'next/router';
import SearchHeader from '../components/SearchHeader';
import Footer from '../components/Footer';
import SearchResults from '../components/Search/SearchResults';
import { getRecentSearches, setRecentSearches, fourSquareAPI } from '../util/helpers';

class Home extends React.Component {
  static async getInitialProps({ query: { query = '', near, ll } }) {
    if (near || ll) {
      const explore = await fourSquareAPI.explore({ query, near, ll });
      return { explore };
    }
    return { explore: '' };
  }

  constructor(props) {
    super(props);
    const { explore, url: { query } } = this.props;
    this.state = {
      requestId: getOr('', 'meta.requestId', explore),
      venues: getOr([], 'response.groups[0].items', explore),
      recentSearches: getRecentSearches(),
      currentSearch: { ...query },
      searchMode: isObject(explore),
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    // execute only in the browser, if it supports geolocation
    if (typeof window !== 'undefined' && 'geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.setState({
          ll: `${position.coords.latitude},${position.coords.longitude}`,
        });
      });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    // we should only update if we have a valid response on our hands
    return nextState.requestId === this.state.requestId;
  }

  handleHistory = pastSearch => this.setState({ ...pastSearch }, () => this.handleSubmit());

  async handleSubmit(e = {}) {
    // we're using the same handler for both the form requests and history requests
    if (e.target instanceof HTMLFormElement) {
      e.preventDefault();
    }
    const {
      query, near, recentSearches, ll,
    } = this.state;
    const { url: { pathname } } = this.props;
    const explore = await fourSquareAPI.explore({ query, near, ll });
    const requestId = getOr('', 'meta.requestId', explore);
    const status = getOr(500, 'meta.code', explore);
    // we should only go ahead with the process if we have a valid response on our hands
    if (requestId && status === 200) {
      const href = {
        pathname,
        query: {
          // only one of near and ll params are required for the request
          // we are writing what we used for the request to the url
          ...pickBy(identity, { query, near, ll }),
        },
      };
      this.setState(
        {
          venues: getOr([], 'response.groups[0].items', explore),
          // keep the last 10 searches
          recentSearches: [{ query, near, requestId }, ...take(9, recentSearches)],
          // we'll use this for the no result error message
          currentSearch: { query, near },
          // this flag decides the header style and whether we'll render results
          searchMode: true,
        },
        () => {
          // keeping this in a callback ensures that the state updated
          // and we won't write old info to localStorage
          Router.replace(href, href, { shallow: true });
          setRecentSearches(this.state.recentSearches);
        },
      );
    }
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  render() {
    const {
      venues, recentSearches, searchMode, currentSearch, ll,
    } = this.state;
    return (
      <React.Fragment>
        <SearchHeader
          compact={searchMode}
          formSubmit={this.handleSubmit}
          inputChange={this.handleChange}
          requireNear={!ll}
        />
        {searchMode ? (
          <SearchResults
            venues={venues}
            recentSearches={recentSearches}
            currentSearch={currentSearch}
            handleHistory={this.handleHistory}
          />
        ) : null}
        <Footer />
      </React.Fragment>
    );
  }
}

Home.propTypes = {
  explore: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({
    groups: PropTypes.arrayOf(PropTypes.shape({
      items: PropTypes.arrayOf(PropTypes.shape({
        venue: PropTypes.shape({
          id: PropTypes.string,
          name: PropTypes.string,
        }),
        stats: PropTypes.shape({
          usersCount: PropTypes.number,
        }),
        price: PropTypes.shape({
          tier: PropTypes.number,
        }),
        photos: PropTypes.shape({
          groups: PropTypes.arrayOf(PropTypes.shape({
            items: PropTypes.arrayOf(PropTypes.shape({
              id: PropTypes.string,
              prefix: PropTypes.string,
              suffix: PropTypes.string,
            })),
          })),
        }),
      })),
    })),
  })]).isRequired,
  url: PropTypes.shape({
    query: PropTypes.shape({
      query: PropTypes.string,
      near: PropTypes.string,
      ll: PropTypes.string,
    }),
  }).isRequired,
};

export default Home;
